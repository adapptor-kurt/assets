# Kurt's Assets

This repository contains files that I used to generate graphics for other projects or tests.

If they're SVG then they probably won't be need again because I would have added them as a vector graphic anyway.
