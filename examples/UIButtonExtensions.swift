// Helper for created themed buttons
extension UIButton {
    public func setBackgroundColor(_ color: UIColor?, for state: UIControlState) {
        let colorImage: UIImage?
        if let color = color {
            UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
            defer { UIGraphicsEndImageContext() }
            
            let context = UIGraphicsGetCurrentContext()!
            context.setFillColor(color.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            colorImage = UIGraphicsGetImageFromCurrentImageContext()
        } else {
            colorImage = nil
        }
        
        self.setBackgroundImage(colorImage, for: state)
    }
}